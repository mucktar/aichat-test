import axios from "axios";

const instance = axios.create({
  baseURL: "http://www.omdbapi.com",
});

export const movieList = (text = "") => {
  return instance
    .get("/", {
      params: {
        apiKey: process.env.NEXT_PUBLIC_OMDB_API_KEY,
        s: text,
      },
    })
    .then((response) => {
      if (response.data.Response === "False") {
        return [];
      }

      return response.data.Search;
    })
    .catch(() => []);
};

export const movieDetail = (id) => {
  return instance
    .get("/", {
      params: {
        apiKey: process.env.NEXT_PUBLIC_OMDB_API_KEY,
        i: id,
      },
    })
    .then((response) => response.data)
    .catch(() => []);
};
