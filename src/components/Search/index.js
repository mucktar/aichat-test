import React, { Component } from "react";
import PropTypes from "prop-types";
import { FormControl, InputGroup, Button } from "react-bootstrap";

import styles from "./search.module.scss";

class SearchForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      term: "",
    };

    this.handleChange = this.handleChange.bind(this);
    this.searchMovie = this.searchMovie.bind(this);
  }

  handleChange(event) {
    const term = event.target.value;

    this.setState({
      term,
    });
  }

  searchMovie() {
    const { handleSearch } = this.props;
    const { term } = this.state;

    if (term) {
      handleSearch(term);
    }
  }

  render() {
    const { title } = this.props;
    const { term } = this.state;

    return (
      <div className={styles.form}>
        <InputGroup>
          <FormControl
            type="text"
            value={term}
            onChange={this.handleChange}
            placeholder={title}
          />
          <InputGroup.Append>
            <Button onClick={this.searchMovie} variant="outline-secondary">
              Search
            </Button>
          </InputGroup.Append>
        </InputGroup>
      </div>
    );
  }
}

SearchForm.propTypes = {
  title: PropTypes.string,
  handleSearch: PropTypes.func.isRequired,
};

SearchForm.defaultProps = {
  title: "Search",
};

export default SearchForm;
