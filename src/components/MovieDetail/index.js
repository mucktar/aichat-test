import React from "react";
import PropTypes from "prop-types";
import { Figure } from "react-bootstrap";

import style from "./detail.module.scss";

const MovieDetail = ({ movie }) => {
  return (
    <Figure className={style.detail}>
      <Figure.Image height={400} src={movie.Poster} />
      <Figure.Caption className={style.caption}>{movie.Title}</Figure.Caption>
      <dl className={style.description}>
        <dt>Year</dt>
        <dd>{movie.Year}</dd>

        <dt>Released</dt>
        <dd>{movie.Released}</dd>

        <dt>Director</dt>
        <dd>{movie.Director}</dd>

        <dt>Plot</dt>
        <dd>{movie.Plot}</dd>

        <dt>Awards</dt>
        <dd>{movie.Awards}</dd>

        <dt>Genre</dt>
        <dd>{movie.Genre}</dd>

        <dt>Actors</dt>
        <dd>{movie.Actors}</dd>

        <dt>Rating</dt>
        <dd>{movie.imdbRating}</dd>
      </dl>
    </Figure>
  );
};

MovieDetail.propTypes = {
  movie: PropTypes.shape({
    Poster: PropTypes.string,
    Title: PropTypes.string,
    Year: PropTypes.string,
    Released: PropTypes.string,
    Director: PropTypes.string,
    Plot: PropTypes.string,
    Awards: PropTypes.string,
    Genre: PropTypes.string,
    Actors: PropTypes.string,
    imdbRating: PropTypes.string,
  }).isRequired,
};

export default MovieDetail;
