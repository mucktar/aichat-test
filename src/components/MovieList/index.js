import React from "react";
import PropTypes from "prop-types";
import { Table } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar as faStarRegular } from "@fortawesome/free-regular-svg-icons";
import { faStar as faStarSolid } from "@fortawesome/free-solid-svg-icons";

import style from "./list.module.scss";

const MovieList = ({ movies, showDetail, toggleFavourite }) => {
  return (
    <Table bordered className={style.list}>
      <thead>
        <tr>
          <th>Title</th>
          <th>Year</th>
          <th>imDB ID</th>
          <th aria-label="Action" />
        </tr>
      </thead>
      <tbody>
        {movies.length === 0 ? (
          <tr>
            <td colSpan="4">No data to display</td>
          </tr>
        ) : (
          movies.map((movie) => (
            <tr key={movie.imdbID}>
              <td>
                <button type="button" onClick={() => showDetail(movie.imdbID)}>
                  {movie.Title}
                </button>
              </td>
              <td>{movie.Year}</td>
              <td>{movie.imdbID}</td>
              <td>
                <button type="button" onClick={() => toggleFavourite(movie)}>
                  <FontAwesomeIcon
                    icon={movie.favourite ? faStarSolid : faStarRegular}
                  />
                </button>
              </td>
            </tr>
          ))
        )}
      </tbody>
    </Table>
  );
};

MovieList.propTypes = {
  movies: PropTypes.array,
  showDetail: PropTypes.func.isRequired,
  toggleFavourite: PropTypes.func.isRequired,
};

MovieList.defaultProps = {
  movies: [],
};

export default MovieList;
