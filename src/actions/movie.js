import * as types from "../constants/types";
import { movieList, movieDetail } from "../api";

function searchResult(term, movies, favourites) {
  return {
    type: types.SEARCH_MOVIE,
    payload: {
      term,
      movies,
      favourites,
    },
  };
}

function movieDetailResult(movie) {
  return {
    type: types.GET_MOVIE_DETAIL,
    payload: {
      movie,
    },
  };
}

export const searchMovie = (term) => {
  return (dispatch, getState) => {
    const favourites = getState().favourite.list.map((movie) => movie.imdbID);
    return movieList(term).then((result) => {
      dispatch(searchResult(term, result, favourites));
    });
  };
};

export const showMovie = (id) => {
  return (dispatch) => {
    return movieDetail(id).then((result) => {
      dispatch(movieDetailResult(result));
    });
  };
};
