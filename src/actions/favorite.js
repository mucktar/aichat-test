import * as types from '../constants/types';

export const addToFavorite = (movie) => ({
  type: types.ADD_TO_FAVORITE,
  payload: { movie },
});

export const removeFromFavorite = (movie) => ({
  type: types.REMOVE_FROM_FAVORITE,
  payload: { movie },
});
