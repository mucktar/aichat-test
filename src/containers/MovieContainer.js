/* eslint-disable react/prop-types */
import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Modal } from "react-bootstrap";
import MovieList from "../components/MovieList";
import MovieDetail from "../components/MovieDetail";
import Search from "../components/Search";

import { searchMovie, showMovie } from "../actions/movie";
import { addToFavorite, removeFromFavorite } from "../actions/favorite";

class MovieContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalOpened: false,
    };

    this.toggleDetail = this.toggleDetail.bind(this);
    this.toggleFavourite = this.toggleFavourite.bind(this);
  }

  toggleDetail(movieId) {
    const { modalOpened } = this.state;
    const { showMovieDetail } = this.props;

    if (!modalOpened) {
      showMovieDetail(movieId).then(() => {
        this.setState({
          modalOpened: true,
        });
      });
    } else {
      this.setState({
        modalOpened: !modalOpened,
      });
    }
  }

  toggleFavourite(movie) {
    const {
      favourite,
      removeMovieFromFavorite,
      addMovieToFavorite,
    } = this.props;

    if (favourite.includes(movie.imdbID)) {
      removeMovieFromFavorite(movie);
    } else {
      addMovieToFavorite(movie);
    }
  }

  render() {
    const { movieList, movieDetail, searchMovieByTitle } = this.props;
    const { modalOpened } = this.state;

    return (
      <>
        <Search
          title="Enter Movie title here"
          handleSearch={searchMovieByTitle}
        />

        <MovieList
          movies={movieList}
          showDetail={this.toggleDetail}
          toggleFavourite={this.toggleFavourite}
        />

        <Modal show={modalOpened} onHide={this.toggleDetail} size="lg">
          <Modal.Body>
            {modalOpened && <MovieDetail movie={movieDetail} />}
          </Modal.Body>
        </Modal>
      </>
    );
  }
}

const mapStateToProps = ({ movie, favourite }) => {
  return {
    searchTerm: movie.search,
    movieList: movie.list,
    movieDetail: movie.detail,
    favourite: favourite.list.map((detail) => detail.imdbID),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    searchMovieByTitle: bindActionCreators(searchMovie, dispatch),
    showMovieDetail: bindActionCreators(showMovie, dispatch),
    addMovieToFavorite: bindActionCreators(addToFavorite, dispatch),
    removeMovieFromFavorite: bindActionCreators(removeFromFavorite, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MovieContainer);
