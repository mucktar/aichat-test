/* eslint-disable react/prop-types */
import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Modal } from "react-bootstrap";
import MovieList from "../components/MovieList";
import MovieDetail from "../components/MovieDetail";

import { removeFromFavorite } from "../actions/favorite";
import { showMovie } from "../actions/movie";

class FavouriteContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalOpened: false,
    };

    this.toggleDetail = this.toggleDetail.bind(this);
  }

  toggleDetail(movieId) {
    const { modalOpened } = this.state;
    const { showMovieDetail } = this.props;

    if (!modalOpened) {
      showMovieDetail(movieId).then(() => {
        this.setState({
          modalOpened: true,
        });
      });
    } else {
      this.setState({
        modalOpened: !modalOpened,
      });
    }
  }

  render() {
    const { favouriteList, movieDetail, removeMovieFromFavorite } = this.props;
    const { modalOpened } = this.state;

    return (
      <>
        <MovieList
          movies={favouriteList}
          showDetail={this.toggleDetail}
          toggleFavourite={removeMovieFromFavorite}
        />

        <Modal show={modalOpened} onHide={this.toggleDetail} size="lg">
          <Modal.Body>
            {modalOpened && <MovieDetail movie={movieDetail} />}
          </Modal.Body>
        </Modal>
      </>
    );
  }
}

const mapStateToProps = ({ favourite, movie }) => {
  return {
    favouriteList: favourite.list,
    movieDetail: movie.detail,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    showMovieDetail: bindActionCreators(showMovie, dispatch),
    removeMovieFromFavorite: bindActionCreators(removeFromFavorite, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FavouriteContainer);
