import * as types from "../constants/types";

const initialState = {
  list: [],
  detail: null,
  search: "",
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.SEARCH_MOVIE:
      return {
        ...state,
        list: payload.movies.map((movie) => {
          return {
            ...movie,
            favourite: payload.favourites.includes(movie.imdbID),
          };
        }),
        search: payload.term,
      };

    case types.GET_MOVIE_DETAIL:
      return {
        ...state,
        detail: payload.movie,
      };

    case types.ADD_TO_FAVORITE:
      return {
        ...state,
        list: state.list.map((movie) => {
          if (movie.imdbID === payload.movie.imdbID) {
            return {
              ...movie,
              favourite: true,
            };
          }

          return movie;
        }),
      };

    case types.REMOVE_FROM_FAVORITE:
      return {
        ...state,
        list: state.list.map((movie) => {
          if (movie.imdbID === payload.movie.imdbID) {
            return {
              ...movie,
              favourite: false,
            };
          }

          return movie;
        }),
      };

    default:
      return state;
  }
};
