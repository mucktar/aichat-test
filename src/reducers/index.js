import { combineReducers } from 'redux';
import favourite from './favorite';
import movie from './movie';

// COMBINED REDUCERS
const reducers = {
  favourite,
  movie,
};

export default combineReducers(reducers);
