import * as types from "../constants/types";

const initialState = {
  list: [],
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.ADD_TO_FAVORITE:
      return {
        list: [
          ...state.list,
          {
            ...payload.movie,
            favourite: true,
          },
        ],
      };

    case types.REMOVE_FROM_FAVORITE:
      return {
        list: [
          ...state.list.filter(
            (movie) => movie.imdbID !== payload.movie.imdbID
          ),
        ],
      };

    default:
      return state;
  }
};
