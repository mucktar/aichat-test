/* eslint-disable react/prop-types */
import React from "react";
import { Provider } from "react-redux";
import { useStore } from "../src/store";
import Layout from "../src/components/Layout";

import "../src/styles/app.scss";

export default function App({ Component, pageProps }) {
  const store = useStore(pageProps.initialReduxState);

  return (
    <Provider store={store}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </Provider>
  );
}
