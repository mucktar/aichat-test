import React from "react";
import { Tabs, Tab } from "react-bootstrap";
import MovieContainer from "../src/containers/MovieContainer";
import FavouriteContainer from "../src/containers/FavouriteContainer";

const Index = () => {
  return (
    <>
      <Tabs defaultActiveKey="search" transition={false}>
        <Tab eventKey="search" title="Search Movie">
          <MovieContainer />
        </Tab>
        <Tab eventKey="favorite" title="My Favorite">
          <FavouriteContainer />
        </Tab>
      </Tabs>
    </>
  );
};

export default Index;
