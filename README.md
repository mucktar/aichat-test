# Running App

### Web UI

Prepare environtment with following command

```sh
  $ copy .env.example .env
```

and fill `NEXT_PUBLIC_OMDB_API_KEY` with your [OMDB] api key.

Execute the following command to run web UI

```sh
$ yarn install
$ yarn run production
```

### Tech

This test app use a number of opensource project to run properly :

- [nextjs] - Pre rendered reactjs
- [redux-thunk] - Enabling asyncrhornous dispatch on redux
- [react-bootstrap] - Bootstrap component for react
- [fortawesone] - Fontawesome icon toolkit for react
- [sass] - CSS extension

[nextjs]: https://github.com/vercel/next.js/
[redux-thunk]: https://github.com/reduxjs/redux-thunk/
[react-bootstrap]: https://github.com/react-bootstrap/react-bootstrap/
[fortawesone]: https://github.com/FortAwesome/Font-Awesome/
[sass]: https://github.com/sass/sass/
[omdb]: http://www.omdbapi.com/apikey.aspx
